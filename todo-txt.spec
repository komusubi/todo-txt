Name:     todo.txt
Version:  2.11.0
Release:  1%{?dist}
Summary:  management todo task.

Group:    Applications/Internet
License:  GENERAL PUBLIC LICENSE v3
URL:      http://todotxt.com/
Source0:  https://github.com/todotxt/todo.txt-cli/releases/download/v${version}/%{name}_cli-%{version}.zip

BuildArch: noarch

Requires: bash-completion 

#SRC_URL https://github.com/todotxt/todo.txt-cli/releases/download/v2.11.0/todo.txt_cli-2.11.0.zip

%description
management todo task lists.

%prep
%setup -q -n %{name}_cli-%{version}

%build
%{nil}

%install
rm -rf %{buildroot}
%{__install} -dm 755 %{buildroot}/usr/bin
%{__install} -dm 755 %{buildroot}%{_sysconfdir}/bash_completion.d
%{__install} -dm 755 %{buildroot}%{_sysconfdir}/skel

%{__install} -pm 755 todo.sh %{buildroot}/usr/bin/todo
%{__install} -pm 644 todo.cfg %{buildroot}%{_sysconfdir}/skel/.todo.cfg
%{__install} -pm 644 todo_completion %{buildroot}%{_sysconfdir}/bash_completion.d/todo
# fix bash completion definition (and must be alias t='todo' in ${HOME}/.bashrc)
%{__sed} -i -e "s/^\(complete \+-F \+_todo \+\)todo.sh$/\1t/" \
            -e "s/\(_todo_sh=\${_todo_sh:-\)todo.sh}/\1todo}/" %{buildroot}%{_sysconfdir}/bash_completion.d/todo

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc
/usr/bin/todo
%{_sysconfdir}/bash_completion.d/todo
%{_sysconfdir}/skel/.todo.cfg

%changelog
* Sat Apr  7 2018 komusubi <komusubi@gmail.com> 2.11.0-1
- update latest version

* Mon Oct 20 2014 komusubi <komusubi@gmail.com> 2.10-1
- update latest version

* Sat May 31 2014 komusubi <komusubi@gmail.com> 2.9-5
- fix default value _todo_sh

* Sat May 31 2014 komusubi <komusubi@gmail.com> 2.9-3
- fix bash-completion (delete definition todo.sh)

* Sat May 31 2014 komusubi <komusubi@gmail.com> 2.9-2
- fix bash-completion 

* Sat May 31 2014 komusubi <komusubi@gmail.com> 2.9-3
- fix install todo.sh.
